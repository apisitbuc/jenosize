module.exports = {
    testEnvironment: 'node',
    transform: {
      '^.+\\.tsx?$': 'ts-jest',
    },
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(ts|js)x?$',
    coverageDirectory: 'coverage',
    coveragePathIgnorePatterns: [
      '<rootDir>/src/index.ts',
      '<rootDir>/src/config*',
      '<rootDir>/src/bootstrap*',
      '<rootDir>/src/api',
      '<rootDir>/src/models',
      '<rootDir>/src/repositories',
      '<rootDir>/src/adapters/services',
      '.interface.ts',
      '.model.ts',
      '.mock.ts',
      '.health.ts',
    ],
    collectCoverageFrom: ['src/**/*.{ts,tsx,js,jsx}', '!src/**/*.d.ts'],
    setupFiles: ['./test/setup.ts'],
  };
  