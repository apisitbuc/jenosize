import bodyParser from "body-parser";
import express, { Request, Response } from "express";
import { GoogleApiService } from "./adapters/services/googleApi.service";
import JenosizeController from "./controllers/jenosizeController";
import FirebaseDomain from "./domain/firebase.domain";
import { Game24Domain } from "./domain/game24.domain";
import apiAuthMiddleware from "./middlewear/apiAuthMiddleware";

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(apiAuthMiddleware);

const googleApiService = new GoogleApiService();
const game24Domain = new Game24Domain();
const firebaseDomain = new FirebaseDomain();

const jenosizeController = new JenosizeController(
  googleApiService,
  game24Domain,
  firebaseDomain
);

app.get("/restaurants", (req: Request, res: Response) =>
  jenosizeController.searchRestaurants(req, res)
);

app.post("/game24", (req: Request, res: Response) =>
  jenosizeController.game24(req, res)
);

app.get("/signInWithGoogle", (req: Request, res: Response) =>
  jenosizeController.signInWithGoogle(req, res)
);

app.get("/signInWithFacebook", (req: Request, res: Response) =>
  jenosizeController.signInWithFacebook(req, res)
);

app.post("/signInWithEmailAndPassword", (req: Request, res: Response) =>
  jenosizeController.signInWithEmailAndPassword(req, res)
);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
