import { Request, Response } from "express";
import { GoogleApiService } from "../adapters/services/googleApi.service";
import { Game24Domain } from "../domain/game24.domain";
import FirebaseDomain from "../domain/firebase.domain";

class JenosizeController {
  constructor(
    private googleApiService: GoogleApiService,
    private game24Domain: Game24Domain,
    private firebaseDomain: FirebaseDomain
  ) {}

  async searchRestaurants(req: Request, res: Response): Promise<void> {
    const { search } = req?.query;

    try {
      const response = await this.googleApiService.searchPlace(search);

      res.send(response);
    } catch (err) {
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  game24(req: Request, res: Response): void {
    const numbers = req.body?.numbers;

    if (numbers.length !== 4) {
      res.status(400).json({ message: "Please provide exactly 4 numbers." });
      return;
    }

    const result = this.game24Domain.game24(numbers);

    res.json({ result });
  }

  signInWithGoogle(req: Request, res: Response): void {
    const response = this.firebaseDomain.signInWithGoogle();
    res.send(response);
  }

  signInWithFacebook(req: Request, res: Response): void {
    const response = this.firebaseDomain.signInWithFacebook();
    res.send(response);
  }


  signInWithEmailAndPassword(req: Request, res: Response): void {
    const { email, password } = req.body;
    const response = this.firebaseDomain.signInWithEmailAndPassword(
      email,
      password
    );
    res.send(response);
  }
}

export default JenosizeController;
