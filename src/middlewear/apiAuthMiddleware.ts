import { Request, Response, NextFunction } from "express";

const apiKey = "mock-api-key"; 

const apiAuthMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const providedApiKey = req.headers["api-key"];

  if (providedApiKey === apiKey) {
    next();
  } else {
    res.status(401).json({ error: "Unauthorized" });
  }
};

export default apiAuthMiddleware;
