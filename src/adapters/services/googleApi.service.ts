import { RestClient } from "../../client/restClient";

export class GoogleApiService {
  private client: RestClient;
  private readonly API_KEY = "mock-api-key";

  constructor() {
    this.client = new RestClient({
      baseURL: "https://maps.googleapis.com/maps/api",
    });
  }

  async searchPlace(query: any) {
    return await this.client.get(`/place/textsearch/json`, {
      params: { query, key: this.API_KEY },
    });
  }
}
