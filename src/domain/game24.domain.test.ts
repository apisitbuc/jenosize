import { Game24Domain } from "./game24.domain";

describe("game 24 Domain", () => {
  const domain = new Game24Domain();
  
  it.each([
    [[2, 4, 6, 8], "YES"],
    [[6, 6, 6, 6], "YES"],
    [[9, 9, 6, 1], "YES"],
  ])("should be 24", (numbers, expected) => {
    expect(domain.game24(numbers)).toEqual(expected);
  });

  it.each([
    [[9, 9, 9, 9], "NO"],
    [[7, 7, 7, 7], "NO"],
    [[1, 1, 1, 1], "NO"],
  ])("should not be 24", (numbers, expected) => {
    expect(domain.game24(numbers)).toEqual(expected);
  });
});
