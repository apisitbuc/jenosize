enum RESULT {
  YES = "YES",
  NO = "NO",
}

export class Game24Domain {
  constructor() {}

  private evaluateExpression(expression: string): number {
    try {
      return eval(expression);
    } catch (error) {
      return NaN;
    }
  }

  private backtrack(
    numbers: number[],
    expression: string,
    result: string[]
  ): void {
    if (numbers.length === 0) {
      if (this.evaluateExpression(expression) === 24) {
        result.push("24");
      }
      return;
    }

    for (let i = 0; i < numbers.length; i++) {
      const newNumbers = numbers.slice(0, i).concat(numbers.slice(i + 1));
      this.backtrack(newNumbers, expression + numbers[i], result);
      this.backtrack(newNumbers, expression + "+" + numbers[i], result);
      this.backtrack(newNumbers, expression + "-" + numbers[i], result);
      this.backtrack(newNumbers, expression + "*" + numbers[i], result);
      this.backtrack(newNumbers, expression + "/" + numbers[i], result);
    }
  }

  game24(numbers: number[]) {
    const result: string[] = [];
    this.backtrack(numbers, "", result);
    return result.includes("24") ? RESULT.YES : RESULT.NO;
  }
}
