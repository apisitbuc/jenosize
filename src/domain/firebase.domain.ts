import { initializeApp } from "firebase/app";
import {
  getAuth,
  signInWithPopup,
  GoogleAuthProvider,
  FacebookAuthProvider,
  signInWithEmailAndPassword as signInWithEmail,
  UserCredential,
  signOut as signOutUser,
} from "firebase/auth";
import { firebaseConfig } from "../config/firebaseConfig";

class FirebaseDomain {
  private auth: any;

  constructor() {
    initializeApp(firebaseConfig);
    this.auth = getAuth();
  }

  signInWithGoogle(): Promise<UserCredential> {
    const provider = new GoogleAuthProvider();
    return signInWithPopup(this.auth, provider);
  }

  signInWithFacebook(): Promise<UserCredential> {
    const provider = new FacebookAuthProvider();
    return signInWithPopup(this.auth, provider);
  }

  signInWithEmailAndPassword(
    email: string,
    password: string
  ): Promise<UserCredential> {
    return signInWithEmail(this.auth, email, password);
  }

  signOut(): Promise<void> {
    return signOutUser(this.auth);
  }
}

export default FirebaseDomain;
