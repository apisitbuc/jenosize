/* eslint-disable @typescript-eslint/no-unused-vars */
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

const DEFAULT_TIMEOUT_MS = 30000;

export interface IRestClientLogger {
  error(obj: Object, ...params: any[]): void;
  info(obj: Object, ...params: any[]): void;
}

export interface IConfig extends AxiosRequestConfig {
  notFoundAsNull?: boolean;
}

export interface IRequest extends AxiosRequestConfig {}

export interface IRestClient {
  request<ResData>(request: IRequest): Promise<ResData | null>;
  get<ResData>(url: string, request: IRequest): Promise<ResData | null>;
}

export type Params = { [key: string]: string | number | boolean };

function handleError(
  error: any,
  config: IConfig = {},
  _logger: IRestClientLogger,
) {
  if (error.response) {
    if (error.response.status === 404 && config.notFoundAsNull === true) {
      return Promise.resolve(null);
    }
  }

  return Promise.reject(error);
}

function handleResponse<ResData>(
  response: AxiosResponse<ResData>,
  _logger: IRestClientLogger,
) {
  return response.data;
}

export class NoopLogger implements IRestClientLogger {
  info(_obj: any, ..._params: any[]) {}
  error(_obj: Object, ..._params: any[]) {}
}

export class RestClient implements IRestClient {
  private axios: AxiosInstance;
  private logger: IRestClientLogger;

  constructor(private config?: IConfig, logger?: IRestClientLogger) {
    this.config = this.buildConfig(config);
    this.axios = axios.create(this.config);
    this.logger = logger || new NoopLogger();
  }

  buildConfig(config?: IConfig) {
    return Object.assign({}, { timeout: DEFAULT_TIMEOUT_MS }, config);
  }

  async request<ResData>(request: IRequest): Promise<ResData | null> {
    try {
      const response = await this.axios.request<ResData>(request);
      return handleResponse(response, this.logger);
    } catch (error) {
      return await handleError(error, this.config, this.logger);
    }
  }

  get<ResData>(url: string, request: IRequest = {}) {
    return this.request<ResData>({
      method: 'GET',
      url,
      ...request,
    });
  }
}
